import 'babel-polyfill'
import Vue from 'vue'
import App from '@/App.vue'
import vuetify from '@/plugins/vuetify';
import store from '@/store';
import VueGeolocation from 'vue-browser-geolocation';
import "@/assets/global.css"

Vue.use(VueGeolocation);

Vue.config.productionTip = false

import VueOffline from 'vue-offline'
var SocialSharing = require('vue-social-sharing');

Vue.use(SocialSharing);
Vue.use(VueOffline)
import router from './router'

new Vue({
  store,
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
