import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const state = {
  message: 'Hellooo',
  username: 'Alish',
  locationMapPositionLat: '',
  locationMapPositionLng: '',
  user: {},
  loadingPage: false,
  createCompanyDialog: false,
  logOrReg: 1,
  workerCalendarWorkerId: null,
  searchedAvailableTime: null,
  reloadVue: 0,
  navBarIcon: null,
  mainTitle: '',
  selectedCompany: {},
  selectedService: {},
  selectedServicer: {},
  selectedServices: {},
  notification: {
    message: null,
  },

};
const getters = {
  welcomeMessage(state) {
    return `${state.message} ${state.username}`;
  }
};
const mutations = {
  setUsername(state, name) {
    state.username = name
  },
  setLocationMapPositionLat(state, locationMapPositionLat) {
    state.locationMapPositionLat = locationMapPositionLat
  },
  setLocationMapPositionLng(state, locationMapPositionLng) {
    state.locationMapPositionLng = locationMapPositionLng
  },
  setCreateCompanyDialog(state, dialog) {
    state.createCompanyDialog = dialog
  },
  setUserData(state, user) {
    state.user = user
  },
  setLoadingPage(state, loading) {
    state.loadingPage = loading
  },
  setlogOrReg(state, logOrReg) {
    state.logOrReg = logOrReg
  },
  setWorkerCalendarWorkerId(state, workerCalendarWorkerId) {
    state.workerCalendarWorkerId = workerCalendarWorkerId
  },
  setSearchedAvailableTime(state, searchedAvailableTime) {
    state.searchedAvailableTime = searchedAvailableTime
  },
  setReloadVue(state, reloadVue) {
    state.reloadVue = reloadVue
  },
  setNavBarIcon(state, navBarIcon) {
    state.navBarIcon = navBarIcon
  },
  setMainTitle(state, mainTitle) {
    state.mainTitle = mainTitle
  },
  setSelectedCompany(state, selectedCompany) {
    state.selectedCompany = selectedCompany
  },
  setSelectedService(state, selectedService) {
    state.selectedService = selectedService
  },
  setSelectedServicer(state, selectedServicer) {
    state.selectedServicer = selectedServicer
  },
  setSelectedServices(state, selectedServices) {
    state.selectedServices = selectedServices
  },
  setNotification(state, notification) {
    state.notification = notification
  },
};
const actions = {
  updateUsername({ commit }, name) {
    commit('setUsername', name)
  },
  updateLocationMapPositionLat({ commit }, locationMapPositionLat) {
    commit('setLocationMapPositionLat', locationMapPositionLat)
  },
  updateLocationMapPositionLng({ commit }, locationMapPositionLng) {
    commit('setLocationMapPositionLng', locationMapPositionLng)
  },
  updatecreateCompanyDialog({ commit }, dialog) {
    commit('setCreateCompanyDialog', dialog)
  },
  updateUserData({ commit }, user) {
    commit('setUserData', user)
  },
  updateLoadingPage({ commit }, loading) {
    commit('setLoadingPage', loading)
  },
  updatelogOrReg({ commit }, logOrReg) {
    commit('setlogOrReg', logOrReg)
  },
  updateWorkerCalendarWorkerId({ commit }, workerCalendarWorkerId) {
    commit('setWorkerCalendarWorkerId', workerCalendarWorkerId)
  },
  updateSearchedAvailableTime({ commit }, searchedAvailableTime) {
    commit('setSearchedAvailableTime', searchedAvailableTime)
  },
  updateReloadVue({ commit }, reloadVue) {
    commit('setReloadVue', reloadVue)
  },
  updateNavBarIcon({ commit }, navBarIcon) {
    commit('setNavBarIcon', navBarIcon)
  },
  updateMainTitle({ commit }, mainTitle) {
    commit('setMainTitle', mainTitle)
  },
  updateSelectedCompany({ commit }, selectedCompany) {
    commit('setSelectedCompany', selectedCompany)
  },
  updateSelectedService({ commit }, selectedService) {
    commit('setSelectedService', selectedService)
  },
  updateSelectedServicer({ commit }, selectedServicer) {
    commit('setSelectedServicer', selectedServicer)
  },
  updateSelectedServices({ commit }, selectedServices) {
    commit('setSelectedServices', selectedServices)
  },
  updateNotification({ commit }, notification) {
    commit('setNotification', notification)
  },
};

const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
})

export default store;
