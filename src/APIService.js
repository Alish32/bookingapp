import axios from 'axios';
//const API_URL = 'http://bookingapp.nspsolutions.net:8080/api';
//const API_URL = 'http://192.168.0.16:8080/api';
const API_URL = 'https://peaceful-waters-86049.herokuapp.com/api';
export class APIService {

    constructor() {
    }
    getUserData(id) {
        const url = `${API_URL}/auth/users/${id}`;
        return axios.get(url).then(response => response.data);
    }
    signIn(user) {
        const url = `${API_URL}/auth/signin/`;
        return axios.post(url, user);
    }
    signUp(details) {
        const url = `${API_URL}/auth/signup/`;
        return axios.post(url, details);
    }
    getUserCompanies(id) {
        const url = `${API_URL}/auth/users/${id}`;
        return axios.get(url).then(response => response.data.data.user.companies);
    }
    createCompany(details, id) {
        const url = `${API_URL}/auth/users/` + id + `/companies`;
        return axios.post(url, details);
    }
    editProfileInfo(details, id) {
        const url = `${API_URL}/auth/users/${id}`;
        return axios.patch(url, details);
    }
    assignServices(details, userId) {
        const url = `${API_URL}/auth/users/` + userId + `/services`;
        return axios.post(url, details);
    }
    assignRole(details, userId) {
        const url = `${API_URL}/auth/users/` + userId + `/roles`;
        return axios.post(url, details);
    }
    removeRole(userId, roleId, ) {
        const url = `${API_URL}/auth/users/` + userId + `/roles/` + roleId;
        return axios.delete(url);
    }
    saveAvailableTimes(details, userId) {
        const url = `${API_URL}/auth/users/` + userId + `/works`;
        return axios.post(url, details);
    }
    getAvailableAndBookedTimes(userId) {
        const url = `${API_URL}/auth/users/${userId}/works`;
        return axios.get(url).then(response => response.data.data);
    }
    getAvailableAndBookedTimesByCompanyId(userId, companyId) {
        const url = `${API_URL}/auth/users/${userId}/companies/${companyId}/works`;
        return axios.get(url).then(response => response.data.data);
    }
    getServicerByQuery(query) {
        const url = `${API_URL}/auth/users/search?query=${query}`;
        return axios.get(url).then(response => response.data);
    }
    getCustomServices(id, showMe) {
        const url = `${API_URL}/auth/users/${id}/services/self?showMe=${showMe}`;
        return axios.get(url).then(response => response.data.data);
    }
    removeCustomService(userId, customServiceId) {
        const url = `${API_URL}/auth/users/` + userId + `/services/` + customServiceId;
        return axios.delete(url);
    }
    getUserByPhoneNumber(phoneNumber) {
        const url = `${API_URL}/auth/users?phoneNumber=${phoneNumber}`;
        return axios.get(url).then(response => response.data);
    }
    sendInvitationToJoinCompany(details, userId) {
        const url = `${API_URL}/auth/users/` + userId + `/services-company`;
        return axios.post(url, details);
    }
    deleteUser(userId) {
        const url = `${API_URL}/auth/users/` + userId;
        return axios.delete(url);
    }
    changePassword(details, id) {
        const url = `${API_URL}/auth/users/${id}/password`;
        return axios.patch(url, details);
    }
    removeWorkerFromCompany(userId, companyId) {
        const url = `${API_URL}/auth/users/${userId}/companies/${companyId}`;
        return axios.delete(url);
    }
    getPendingBookingRequests(userId) {
        const url = `${API_URL}/organization/users/${userId}/booking/requests`;
        return axios.get(url).then(response => response);
    }
    getPendingBookingRequestsByWorkerId(workerId) {
        const url = `${API_URL}/organization/workers/${workerId}/booking/requests`;
        return axios.get(url).then(response => response);
    }
    acceptBookingRequest(requestId) {
        const url = `${API_URL}/organization/booking/requests/` + requestId + `/accept`;
        return axios.put(url);
    }
    rejectBookingRequest(requestId, message) {
        const url = `${API_URL}/organization/booking/requests/` + requestId + `/reject`;
        return axios.put(url, message);
    }
    getRejectedRequests(userId) {
        const url = `${API_URL}/organization/users/${userId}/booking/rejected-requests`;
        return axios.get(url).then(response => response);
    }
    sendServiceRequest(details) {
        const url = `${API_URL}/organization/service/requests`;
        return axios.post(url, details);
    }
    getServiceRequests() {
        const url = `${API_URL}/organization/service/requests`;
        return axios.get(url).then(response => response);
    }
    acceptServiceRequest(requestId) {
        const url = `${API_URL}/organization/service/requests/` + requestId + `/accept`;
        return axios.put(url);
    }
    rejectServiceRequest(requestId) {
        const url = `${API_URL}/organization/service/requests/` + requestId + `/reject`;
        return axios.put(url);
    }
    deleteCompany(id) {
        const url = `${API_URL}/organization/companies/${id}`;
        return axios.delete(url);
    }
    deleteService(id) {
        const url = `${API_URL}/organization/services/${id}`;
        return axios.delete(url);

    }
    getServices() {
        const url = `${API_URL}/organization/services`;
        return axios.get(url).then(response => response.data.data.services);
    }
    createService(details) {
        const url = `${API_URL}/organization/services`;
        return axios.post(url, details);
    }
    getCompanies(query) {
        const url = `${API_URL}/organization/companies/search?query=${query}`;
        return axios.get(url).then(response => response.data);
    }
    getServicesByQuery(query) {
        const url = `${API_URL}/organization/services/search?query=${query}`;
        return axios.get(url).then(response => response.data);
    }
    getEmployeesByCompanyIdAndServiceId(companyId, serviceId) {
        const url = `${API_URL}/organization/companies/${companyId}/services/${serviceId}/employees`;
        return axios.get(url).then(response => response.data.data);
    }
    getCompanyByServiceId(serviceId) {
        const url = `${API_URL}/organization/services/${serviceId}/companies`;
        return axios.get(url).then(response => response.data.data);
    }
    getEmployeesByServiceId(serviceId) {
        const url = `${API_URL}/organization/services/${serviceId}/employees`;
        return axios.get(url).then(response => response.data.data);
    }
    getCompaniesByEmployeeId(employeId) {
        const url = `${API_URL}/organization/employees/${employeId}/companies`;
        return axios.get(url).then(response => response.data.data);
    }
    getEmployeesByCompanyId(id) {
        const url = `${API_URL}/organization/companies/${id}/employees`;
        return axios.get(url).then(response => response.data.data);
    }
    getServicesByCompanyId(id) {
        const url = `${API_URL}/organization/companies/${id}/services`;
        return axios.get(url).then(response => response.data.data);
    }
    saveBookingTimes(details) {
        const url = `${API_URL}/organization/booking/requests`;
        return axios.post(url, details);
    }
    getCompanyById(id) {
        const url = `${API_URL}/organization/companies/${id}`;
        return axios.get(url).then(response => response.data);
    }
}