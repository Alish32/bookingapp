import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home.vue'
import SignIn from '@/views/Auth/SignIn.vue'
import SignUp from '@/views/Auth/SignUp.vue'
import Admin from '@/views/Admin.vue'
import Company from '@/views/Company.vue'
import CompanyProfile from '@/views/CompanyProfile.vue'
import Service from '@/views/Service.vue'
import AddSelfService from '@/views/AddSelfService.vue'
import LocationMap from '@/components/LocationMap.vue'
import SendInvitation from '@/views/SendInvitation.vue'
import NotFoundComponent from '@/views/NotFoundComponent.vue'
import MyCalendar from '@/views/Calendars/MyCalendar.vue'
import WorkerCalendar from '@/views/Calendars/WorkerCalendar.vue'
import WorkerCalendarClientSide from '@/views/Calendars/WorkerCalendarClientSide.vue'
import EditUserProfile from '@/views/Auth/EditUserProfile.vue'
import SelectedCompany from '@/views/SelectedCompany.vue'
import SelectService from '@/views/SelectService.vue'
import SelectServicer from '@/views/SelectServicer.vue'
import SelectCompany from '@/views/SelectCompany.vue'
import SelectedService from '@/views/SelectedService.vue'
import SelectedServicer from '@/views/SelectedServicer.vue'
import BookingRequests from '@/views/BookingRequests.vue'
import Favorites from '@/views/Favorites.vue'
import MyBookings from '@/views/MyBookings.vue'

import store from '@/store.js'


Vue.use(Router)

const router = new Router({
  base: process.env.BASE_URL,
  routes: [
    { name: "SignIn", path: '/signin', component: SignIn, },
    { name: "SignUp", path: '/signup', component: SignUp, },
    { name: "EditUserProfile", path: '/edit-profile', component: EditUserProfile, meta: { auth: true, admin: false } },
    { name: "Home", path: '/', component: Home, meta: { auth: true, admin: false } },
    { path: '/home', component: Home, meta: { auth: true, admin: false } },
    { name: "Company", path: '/company', component: Company, meta: { auth: true, admin: false } },
    { name: "CompanyProfile", path: '/company-profile/:id', component: CompanyProfile, meta: { auth: true, admin: false } },
    { name: "Service", path: '/service', component: Service, meta: { auth: true, admin: false } },
    { name: "AddSelfService", path: '/add-self-service', component: AddSelfService, meta: { auth: true, admin: false } },
    { name: "Admin", path: '/admin', component: Admin, meta: { auth: true, admin: true } },
    { name: "LocationMap", path: '/location-map', component: LocationMap, meta: { auth: false, admin: false } },
    { name: "SendInvitation", path: '/send-invitation/:id', component: SendInvitation, meta: { auth: false, admin: false } },
    { name: "MyCalendar", path: '/my-calendar', component: MyCalendar, meta: { auth: false, admin: false } },
    { name: "WorkerCalendar", path: '/worker-calendar', component: WorkerCalendar, meta: { auth: false, admin: false } },
    { name: "WorkerCalendarClientSide", path: '/wk-cs', component: WorkerCalendarClientSide, meta: { auth: false, admin: false } },
    { name: "SelectedCompany", path: '/selected-company', component: SelectedCompany, meta: { auth: false, admin: false } },
    { name: "SelectedService", path: '/selected-service', component: SelectedService, meta: { auth: false, admin: false } },
    { name: "SelectedServicer", path: '/selected-servicer', component: SelectedServicer, meta: { auth: false, admin: false } },
    { name: "SelectService", path: '/select-service', component: SelectService, meta: { auth: false, admin: false } },
    { name: "SelectServicer", path: '/select-servicer', component: SelectServicer, meta: { auth: false, admin: false } },
    { name: "SelectCompany", path: '/select-company', component: SelectCompany, meta: { auth: false, admin: false } },
    { name: "BookingRequests", path: '/booking-requests', component: BookingRequests, meta: { auth: false, admin: false } },
    { name: "Favorites", path: '/favorites', component: Favorites, meta: { auth: false, admin: false } },
    { name: "MyBookings", path: '/my-bookings', component: MyBookings, meta: { auth: false, admin: false } },
    { name: "NotFoundComponent", path: '*', component: NotFoundComponent },
  ],
  mode: 'history'
})

router.beforeEach((to, from, next) => {
  store.dispatch('updateLoadingPage', true)

  var authed = false
  var authedAdmin = false
  const authRequired = to.matched.some((route) => route.meta.auth)
  const adminAuthRequired = to.matched.some((route) => route.meta.admin)

  if (to.path == "/signin" && localStorage.getItem('token')) { next("/") }
  if (localStorage.getItem('token') !== null) { authed = true } else { authed = false }
  if (localStorage.getItem('isAdmin') == 'ADMIN') { authedAdmin = true } else { authedAdmin = false }

  if (authRequired && !authed) {
    next('/signin')
  }
  else if (adminAuthRequired && !authedAdmin) {
    next('/home')
  } else {
    next()
  }
})

router.afterEach(() => {
  store.dispatch('updateLoadingPage', false)
})

export default router